#!/bin/bash
i=1
while read site;
do
	mkdir dir-$i
	cd dir-$i
	wget $site
	cd ..
	i=$(($i+1))
done < "$1"
