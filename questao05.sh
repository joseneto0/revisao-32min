#!/bin/bash
opcao=0
while [ $opcao -ne 5 ]; do
	echo "1 - Digite o nome de um arquivo"
	echo "2 - Criar um arquivo"
	echo "3 - Adicionar um número no final do arquivo"
	echo "4 - Remover o arquivo"
	echo "5 - Sair do programa"
	read -p "Digite a opcao: " opcao
	case $opcao in
		1) read nome;;
		2) touch $nome;;
		3) read -p "Digite o número que você quer: " numero; echo $numero >> $nome;;
		4) rm $nome &> /dev/null;;
		5) echo "Obrigado por utilizar o programa :)" && exit 0;;
		*) echo "Digite uma opção válida";;
	esac
done
